#!/bin/bash

MINIKUBE_IP=$(minikube ip)
REQUIRED_HOSTS=$(kubectl get ingress --all-namespaces -o jsonpath='{.items[*].spec.rules[*].host}' | tr ' ' '\n' | uniq)

while read -r HOSTNAME; do 
	sudo hosts remove $HOSTNAME --force 
done <<< "$REQUIRED_HOSTS";

while read -r HOSTNAME; do 
	sudo hosts add $MINIKUBE_IP $HOSTNAME 
	sudo hosts add ::1 $HOSTNAME 
done <<< "$REQUIRED_HOSTS"

hosts list