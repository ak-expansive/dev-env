#!/bin/bash

SRC_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
. $SRC_DIR/output.sh



if [ -z "$NAMESPACE" ]; then
	_error "No NAMESPACE is set for creating pull secrets."
fi

function main() {
	kubectl create secret docker-registry gitlabpullsecret \
		-n $NAMESPACE \
		--docker-server=registry.gitlab.com \
		--docker-username="$(git config user.email)" \
		--docker-password="$(cat $SRC_DIR/../.secrets/gitlab.pw)" \
		--docker-email="$(git config user.email)" || true
}

main "$@"