#!/bin/bash

SRC_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && cd .. && pwd )"

HELM_DIR=$(cd $SRC_DIR/../helm-charts && pwd)

MINIKUBE_CLUSTER_MEMORY=${MINIKUBE_CLUSTER_MEMORY:-8192}
MINIKUBE_CLUSTER_CPUS=${MINIKUBE_CLUSTER_CPUS:-4}

LOGS_NAMESPACE=kube-logs
SHARED_NAMESPACE=expansive--shared

function get_driver_args() {
	DRIVER=$(minikube config get driver)

	if [ "$DRIVER" != "hyperkit" ]; then
		echo ""
		return 1
	fi

	# It's hyperkit

	SHARE_PATH=$(cd $SRC_DIR && cd ../ && pwd)
	echo "--nfs-share $SHARE_PATH"
}

function start() {
	DRIVER_ARGS=$(get_driver_args)

	minikube start --cpus $MINIKUBE_CLUSTER_CPUS --memory $MINIKUBE_CLUSTER_MEMORY $DRIVER_ARGS
	minikube addons enable ingress
	kubectl rollout status -n kube-system deployments/ingress-nginx-controller
}

function create_postgres() {
	helm upgrade --install \
		-f $SRC_DIR/helm-configs.d/postgres.yaml \
		-n $SHARED_NAMESPACE \
		rdb \
		bitnami/postgresql
}

function create_redis_cluster() {
	helm upgrade --install \
		-f $SRC_DIR/helm-configs.d/rediscluster.yaml \
		-n $SHARED_NAMESPACE \
		redis \
		$HELM_DIR/rediscluster
}

function create_elastic_cluster() {
	helm upgrade --install \
	-f $SRC_DIR/helm-configs.d/elasticsearch.yaml \
	-n $SHARED_NAMESPACE \
	es \
	elastic/elasticsearch
}

function destroy() {
	minikube delete
}

function logs() {
	kubectl get ns $LOGS_NAMESPACE &> /dev/null
	_GET_NS_RESULT="$?"

	if [ "$_GET_NS_RESULT" != "0" ]; then
		kubectl create ns $LOGS_NAMESPACE
	fi

	helm repo add elastic https://helm.elastic.co
	helm repo add kiwigrid https://kiwigrid.github.io

	helm upgrade --install \
	-f $SRC_DIR/pefk-stack.d/elasticsearch.yaml \
	-n $LOGS_NAMESPACE \
	logs-es \
	elastic/elasticsearch

	helm upgrade --install \
	-f $SRC_DIR/pefk-stack.d/fluentd-es.yaml \
	-n $LOGS_NAMESPACE \
	fluentd-es \
	kiwigrid/fluentd-elasticsearch

	helm upgrade --install \
	-f $SRC_DIR/pefk-stack.d/kibana.yaml \
	-n $LOGS_NAMESPACE \
	kibana \
	elastic/kibana

	$SRC_DIR/utils.d/hosts.sh
}

function init_shared_helm_repos() {
	helm repo add stable https://kubernetes-charts.storage.googleapis.com/
	helm repo add bitnami https://charts.bitnami.com/bitnami
	helm repo add elastic https://helm.elastic.co
	helm repo update
}

function shared_components() {
	kubectl get ns $SHARED_NAMESPACE &> /dev/null
	_GET_NS_RESULT="$?"

	if [ "$_GET_NS_RESULT" != "0" ]; then
		kubectl create ns $SHARED_NAMESPACE
	fi

	NAMESPACE=$SHARED_NAMESPACE $SRC_DIR/utils.d/create-pull-secrets.sh

	init_shared_helm_repos
	create_postgres
	create_redis_cluster
	create_elastic_cluster

	$SRC_DIR/utils.d/hosts.sh
}

function main() {
	# If an argument is supplied it should be the name of a function in here
	# This will allow us to call specific functions or full startup
	if [ "$#" -gt "0" ]; then
		$1
		exit "$?"
	fi

	start
	logs
	shared_components
}

main "$@"