#!/bin/bash

# See here (hyperkit specific): https://minikube.sigs.k8s.io/docs/handbook/mount/#driver-mounts
NFS_SHARE_ROOT="/nfsshares"

# This should be an absolute path for this host
# i.e /Users/user/some/dir/some/path
HOSTPATH=$1
DRIVER=$(minikube config get driver)

if [ "$DRIVER" != "hyperkit" ]; then
	# On OSX hyperkit is the only one that requires any modification
	# This can be found here: https://minikube.sigs.k8s.io/docs/handbook/mount/#driver-mounts
	# If supporting other linux distros this will need to change
	echo "$1"
	return 1
fi

echo "$NFS_SHARE_ROOT$HOSTPATH"
