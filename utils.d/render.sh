#!/bin/bash

SRC_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && cd .. && pwd )"

IMAGE=registry.gitlab.com/ak-expansive/render:latest

# First argument is the template path
# It is relative to the ak-expansive directory which contains this
# All other arguments are passed to the render function
function main() {
	SHARE_PATH=$(cd $SRC_DIR && cd ../ && pwd)
	TO_MOUNT=$($SRC_DIR/utils.d/hostpaths.sh $SHARE_PATH)
	TEMPLATE="$1"
	shift

	# Run this in a subshell to avoid changing the docker env that the user may have
	# Otherwise they'd have to reset their docker env after this runs
	#
	# We run this on the minikube docker host to avoid requiring dcker and minikube to be running simultaneously
	# They can be quite the memory/CPU hogs
	(eval $(minikube docker-env) &&\
		cat $SRC_DIR/.secrets/gitlab.pw | docker login registry.gitlab.com --password-stdin --username $(git config user.email) > /dev/null &&\
		docker run --rm -v "$TO_MOUNT:/src-code" -w /src-code $IMAGE sh -c "cat $TEMPLATE | render $@"
	)
}

main "$@"