#!/bin/bash

_BASE_KUBECTL_COMMAND="kubectl -n $NAMESPACE"

function _extractpodname() {
	echo "$1" | tr ':' ' ' | awk '{ printf $1 }'
}

function _extractpodindex() {
	INDEX=$(echo "$1" | tr ':' ' ' | awk '{ printf $2 }')

	if [ -z "$INDEX" ]; then
		INDEX="0"
	fi

	echo "$INDEX"
}

function _getpodnamebylabel() {
	INDEX=${2:-0}
	$_BASE_KUBECTL_COMMAND get pod -l name=$1 -o go-template --template "{{ (index .items $INDEX).metadata.name}}"
}

function _get() {
	$_BASE_KUBECTL_COMMAND get "$1" ${@:2}
}

function _ssh() {
	POD_ALIAS=$(_extractpodname "$1")
	POD_NAME=$POD_ALIAS
	
	# If there is an alias for the pod use it
	if typeset -f "_handle_alias" > /dev/null; then
		POD_NAME=$(_handle_alias "$POD_NAME")
	fi

	EXEC_COMMAND="sh"

	if typeset -f "_get_exec_command" > /dev/null; then
		EXEC_COMMAND=$(_get_exec_command "$POD_ALIAS" "$2")
	fi

	POD_INDEX=$(_extractpodindex "$1")

	POD_NAME=$(_getpodnamebylabel "$POD_NAME" "$POD_INDEX")

	CONTAINER_OPT=""

	if [ ! -z "$2" ]; then
		CONTAINER_OPT="-c $2"
	fi

	if [ ! -z "$3" ]; then
		EXEC_COMMAND=${@:3}
	fi

	$_BASE_KUBECTL_COMMAND exec -it $POD_NAME $CONTAINER_OPT -- $EXEC_COMMAND
}

function _tail() {
	POD_NAME=$(_extractpodname "$1")
	
	# If there is an alias for the pod use it
	if typeset -f "_handle_alias" > /dev/null; then
		POD_NAME=$(_handle_alias "$POD_NAME")
	fi

	POD_INDEX=$(_extractpodindex "$1")

	POD_NAME=$(_getpodnamebylabel "$POD_NAME" "$POD_INDEX")

	CONTAINER_OPT=""

	if [ ! -z "$2" ]; then
		CONTAINER_OPT="-c $2"
	fi 

	$_BASE_KUBECTL_COMMAND logs -f $POD_NAME $CONTAINER_OPT
}

function usage() {
	echo "Usage: ./kube [command] [arguments]"
	echo ""
	echo "Commands:"
	echo -e "\tget [resource] [options]"
	echo -e "\t\tDisplays resources from the namespace."
	echo -e "\t\tAll options are passed directly to 'kubectl get' run 'kubectl get -h' to see what's available."
	echo ""
	echo -e "\tssh [pod:index] [?container] [?command]"
	echo -e "\t\tExecs to a container in the namespace, or runs the command if one is provided."
	if typeset -f "_ssh_examples" > /dev/null; then
		echo ""
		echo -e "\t\tExamples:"
		echo ""
		_ssh_examples
	fi
	echo ""
	echo -e "\tail [pod:index] [?container]"
	echo -e "\t\tDisplay logs from a container in the namespace"
	if typeset -f "_tail_examples" > /dev/null; then
		echo ""
		echo -e "\t\tExamples:"
		echo ""
		_tail_examples
	fi
	echo ""
	echo "pod:index"
	echo -e "\tThis format allows you to choose which pod you want to interact with when more than one may exist."
	echo -e "\tFor example a statefulset may consist of 3 pods (pod-0,pod-1,pod-2)."
	echo -e "\tSo to interact with the pod-0 you would use pod:0 in the command argument."
	echo ""
	echo -e "\t\tExamples:"
	echo ""
	echo -e "\t\t./kube ssh pod:0 container"
	echo -e "\t\t./kube tail pod:2 container"
	echo ""
}

function _handle_command() {
	COMMAND=$1
	case "$COMMAND" in
		'get'|'ssh'|'tail')
			_$COMMAND ${@:2}

			exit "$?"
			;;
		*)
			usage
			exit 1
			;;
	esac
}