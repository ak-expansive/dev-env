#!/bin/bash

COLOUR_RED='\033[0;31m'
COLOUR_CYAN='\033[0;36m'
COLOUR_YELLOW='\033[0;93m'
COLOUR_BLUE='\033[0;94m'
NO_COLOUR='\033[0m' # No Color

function _section_header() {
	printf "$COLOUR_CYAN--- Starting: $1 ---$NO_COLOUR"
	echo ""
	echo ""
}

function _section_footer() {
	echo ""
	printf "$COLOUR_CYAN--- Finished: $1 ---$NO_COLOUR"
	echo ""
	echo ""
}

function _info() {
	printf "$COLOUR_BLUE$1$NO_COLOUR"
	echo ""
}

function _error() {
	printf "$COLOUR_RED$1$NO_COLOUR"
	echo ""
}